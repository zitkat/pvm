//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {0, 1, 0, 1.0};
//+
Point(3) = {1, 2, 0, 1.0};
//+
Point(4) = {2, 3, 0, 1.0};
//+
Point(5) = {3, 4, 0, 1.0};
//+
Point(6) = {3, 3, 0, 1.0};
//+
Point(7) = {2, 2, 0, 1.0};
//+
Point(8) = {1, 1, 0, 1.0};
//+


//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 1};
//+
Curve Loop(1) = {3, 4, 5, 6, 7, 8, 1, 2};
//+
Plane Surface(1) = {1};
//+
Extrude {0, 0, .1} {
  Surface{1}; Layers{1}; Recombine;
}
//+
Curve Loop(2) = {17, -19, -2, 44};
//+
Curve Loop(3) = {36, 15, -40, -8};
//+
Plane Surface(51) = {2};
//+
Plane Surface(52) = {3};
//+
Curve Loop(4) = {11, -24, -4, 20};
//+
Curve Loop(5) = {13, -32, -6, 28};
//+
Plane Surface(53) = {4, 5};
//+
Curve Loop(6) = {10, -20, -3, 19};
//+
Plane Surface(54) = {6};
//+
Curve Loop(7) = {32, 14, -36, -7};
//+
Plane Surface(55) = {7};
//+
Curve Loop(8) = {44, -16, -40, 1};
//+
Plane Surface(56) = {8};
//+
Curve Loop(9) = {24, 12, -28, -5};
//+
Plane Surface(57) = {9};
//+
Physical Surface("periodicBoundarytop_patch0") = {49};
//+
Physical Surface("periodicBoundarybot_patch0") = {41};
//+
Physical Surface("periodicBoundarytop_patch2") = {25};
//+
Physical Surface("periodicBoundarybot_patch2") = {33};
//+
Physical Surface("fixedWalltop_patch1") = {21};
//+
Physical Surface("fixedWallbot_patch1") = {37};
//+
Physical Surface("inletBoundary_patch0") = {45};
//+
Physical Surface("outletBoundary_patch2") = {29};
//+
//+
Surface Loop(1) = {21, 49, 41, 45, 1, 50, 37, 33, 25, 29};
//+
Volume(2) = {1};
//+
Physical Volume("internal") = {1};
//+
Physical Volume("internal") += {1};
//+
Physical Surface("front") = {50};
//+
Physical Surface("back") = {1};
